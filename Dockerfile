FROM debian:stable

MAINTAINER paulinemaudet.dev@gmail.com

RUN apt-get update && apt-get upgrade -y && apt-get install -y apache2 telnet elinks openssh-server

COPY index.html /var/www/html/

EXPOSE 80

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
